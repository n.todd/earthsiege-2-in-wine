# Earthsiege 2 in Wine on Linux

## NOTE

Please read the entire set of instructions before proceeding.  I provide no warranty or any guarantee that this will work right.  In theory, this simply replaces the virtual memory size check conditional jumps with unconditional jumps in the game's executable.  It should do nothing else.

Also, make sure you're using the right patch for your version of the game.  The original version and the version released by HiRez Studios are version 1.0.  There's a version on Archive.org (and I'm also pretty sure it's the version in a giant plastic tub of old games I have in storage somewhere) that's updated and is version 1.11.  At some point I'll have to add md5sum information for the different versions.

## The problem

Are you seeing the following error when trying to play Earthsiege II in Wine on Linux?

> You don't have enough virtual memory to run Earthsiege II.  
> Try freeing up some hard drive space from the drive windows  
> is on, or close some applications if there are others running.  
> If you have your virtual memory swapfile set to a fixed size,  
> you may need to increase it.

Well, this is my attempt at fixing it.

## Getting the game

At some point in history, HiRez Studios released a bunch of the old Earthsiege-series games as freeware, including Earthsiege 2.  While the original download page is gone now, various places still host the ISO file that was available there, likely including archive.org

## Getting the game installed

In order to get Earthsiege 2 to install in Wine, mount the ISO using a program like CDEmu.  Alternatively, if you actually have the original CD-ROM and an optical drive, just put it in!  Then, using the program Lutris, which is an excellent Wine frontend, install the game exactly as you would have back in the day.  *Do not use the modern Windows install file from the SierraHelp page or anywhere else*.  It's not needed.  Wine often has better Windows compatibility than Windows.  When the installation is complete, go ahead and try to run the game.  I recommend setting Lutris up to use Gamescope so that the game doesn't clobber your desktop resolution.  If the game works, then hooray, no additional steps are necessary!  If not, go ahead and continue with this process.

## Getting the game to skip the virtual memory check

Clone this repo or download the `ES.EXE-novirtmemcheck.vcdiff` file.  Copy the file into the directory you installed Earthsiege 2 into, likely something like `~/Games/earthsiege-2/drive_c/SIERRA/ES2`.  Then, move `ES2.EXE` to something like `ES2.EXE.bak`.  Now, install the `xdelta3` program from your distribution's repositories, and in a terminal, run `xdelta3.exe -d -s ES2.EXE.bak ES.EXE-novirtmemcheck.vcdiff ES.EXE` in the directory that Earthsiege 2 is installed in.  Go ahead and try to play the game in Lutris again, it should skip the memory check now!

## Now I have a CD error, though!

I told you not to use the modern Windows installer file!  If this is happening for any other reason, I have no clue, I'm sorry.

## TODO

Eventually, I should turn this into an entire Lutris install script.  That would be neat, then it would be a much easier process!